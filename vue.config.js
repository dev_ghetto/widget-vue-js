// const JavaScriptObfuscator = require('webpack-obfuscator');

module.exports = {
  configureWebpack: {
    optimization: {
      splitChunks: false
    },
    // TODO: найти способ обфусцировать код
    // сейчас плагин webpack-obfuscator не работает со Vue
    // plugins: [
    //   new JavaScriptObfuscator({})
    // ]
  },
  css: {
    extract: false,
  }
}
