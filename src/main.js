import Vue from 'vue'
import App from './App.vue'
import VueTheMask from 'vue-the-mask'

Vue.use(VueTheMask);
Vue.config.productionTip = false;

window.runJentuWidget = function() {
  if (window && window.jentuWidget) {
  
    // Creating div for widget
    const elem = document.createElement('div');
    elem.id = `widget_${window.jentuWidget.id}`;
    document.body.appendChild(elem);
  
    // VUE init
    new Vue({
      render: h => h(App, {
        props: {
          widgetId: window.jentuWidget.organisationId,
        }
      }),
    }).$mount(`#widget_${window.jentuWidget.id}`)

  }
}
